import i18n from "i18next";
import { required, email, length, format } from "redux-form-validators";

export default {
  required: required({ message: i18n.t("formError:required") }),
  email: email({ message: i18n.t("formError:email") }),
  maxLength: (max) =>
    length({ max, message: i18n.t("formError:maxLength", { max }) }),
  minLength: (min) =>
    length({ min, message: i18n.t("formError:minLength", { min }) }),
  noWhiteSpaces: format({
    with: /^\S*$/,
    message: i18n.t("formError:noWhiteSpaces"),
  }),
};
