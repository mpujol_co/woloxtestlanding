import { REFERENCES } from "~constants/homeSections";
import { ORDER_TYPE } from "~constants/order";
import { ROUTES } from "~constants/routes";

export const scrollToId = (id) => () => {
  if (REFERENCES[id] && REFERENCES[id].current) {
    window.scrollTo({
      top: REFERENCES[id].current.offsetTop,
      behavior: "smooth",
    });
  }
};

export const dynamicsort = (key, type = ORDER_TYPE.ASC) => {
  const SORT_ORDER = type === ORDER_TYPE.ASC ? 1 : -1;
  return (a, b) =>
    a[key].toLowerCase() < b[key].toLowerCase()
      ? -1 * SORT_ORDER
      : a[key].toLowerCase() > b[key].toLowerCase()
      ? 1 * SORT_ORDER
      : 0;
};

export const getPathname = (routeId) =>
  ROUTES.find(({ id }) => id === routeId).path;

export const fadeUpScroll = (positionTop = 0) => {
  const CLASSNAME = "visible";

  [...document.getElementsByTagName("section")].forEach((CURRENT) => {
    if (
      positionTop + window.innerHeight * 0.8 >= CURRENT?.offsetTop &&
      !CURRENT.classList.contains(CLASSNAME)
    ) {
      CURRENT.classList.add(CLASSNAME);
    }
  });
};
