import { func, oneOfType, shape, instanceOf, string } from "prop-types";

export const customRefProptype = oneOfType([
  func,
  shape({ current: instanceOf(Element) }),
]).isRequired;

export const scrollToProptype = func.isRequired;

export const inputProptype = {
  label: string,
  type: string,
  placeholder: string,
  className: string,
};
