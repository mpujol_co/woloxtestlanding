import { lazy } from "react";

const Home = lazy(() => import("~screens/Home"));
const Login = lazy(() => import("~screens/Login"));
const Dashboard = lazy(() => import("~screens/Dashboard"));

export const ROUTES_IDS = {
  HOME: "home",
  LOGIN: "login",
  DASHBOARD: "dashboard",
};

export const ROUTES = [
  {
    id: ROUTES_IDS.HOME,
    component: Home,
    path: "/",
  },
  {
    id: ROUTES_IDS.LOGIN,
    component: Login,
    path: "/login",
  },
  {
    id: ROUTES_IDS.DASHBOARD,
    component: Dashboard,
    path: "/dashboard",
    isPrivate: true,
  },
];
