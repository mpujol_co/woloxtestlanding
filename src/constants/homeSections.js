import i18n from "i18next";
import { createRef } from "react";
import Home from "~screens/Home/components/Home";
import Technologies from "~screens/Home/components/Technologies";
import Benefits from "~screens/Home/components/Benefits";
import Requirements from "~screens/Home/components/Requirements";

const HOME_REF = createRef();
const TECHNOLOGIES_REF = createRef();
const BENEFITS_REF = createRef();
const REQUERIMENTS_REF = createRef();

export const SECTIONS_IDS = {
  HOME: "home",
  TECHNOLOGIES: "technologies",
  BENEFITS: "benefits",
  REQUERIMENTS: "requirements",
};

export const SECTIONS = [
  {
    id: SECTIONS_IDS.HOME,
    component: Home,
    name: i18n.t("sectionTitle:home"),
    ref: HOME_REF,
  },
  {
    id: SECTIONS_IDS.TECHNOLOGIES,
    component: Technologies,
    name: i18n.t("sectionTitle:technologies"),
    ref: TECHNOLOGIES_REF,
  },
  {
    id: SECTIONS_IDS.BENEFITS,
    component: Benefits,
    name: i18n.t("sectionTitle:benefits"),
    ref: BENEFITS_REF,
  },
  {
    id: SECTIONS_IDS.REQUERIMENTS,
    component: Requirements,
    name: i18n.t("sectionTitle:requirements"),
    ref: REQUERIMENTS_REF,
  },
];

export const REFERENCES = SECTIONS.reduce(
  (acum, item) =>
    acum ? { ...acum, [item.id]: item.ref } : { [item.id]: item.ref },
  {}
);
