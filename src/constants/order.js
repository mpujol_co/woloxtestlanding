export const ORDER_TYPE = {
  ASC: "ASC",
  DESC: "DESC",
};

export const FILTER_KEYS = {
  TECH: "tech",
  YEAR: "year",
  AUTHOR: "author",
  LICENSE: "license",
  LANGUAGE: "language",
  TYPE: "type",
};
