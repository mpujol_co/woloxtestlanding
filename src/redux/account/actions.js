import {
  completeTypes,
  createTypes,
  withPostFailure,
  withPostSuccess,
} from "redux-recompose";
import { reset, SubmissionError } from "redux-form";
import i18n from "i18next";
import { sendData, getData } from "~services/account";
import { set as setLocalstorage } from "~services/localstorage";
import { FORM_NAME, FIELD_NAMES } from "~screens/Login/constants";
import { LOGIN_KEY } from "~constants/localstorage";
import { FILTER_KEYS } from "~constants/order";
import { dynamicsort } from "~utils";
import { TARGET, TYPES } from "./constants";

export const completedTypes = completeTypes(TYPES);
export const actions = createTypes(completedTypes, "@@ACCOUNT");

export const actionCreators = {
  send: (values) => {
    return {
      type: actions.SEND,
      target: TARGET.loginForm,
      payload: values,
      service: sendData,
      successSelector: ({ token }) => token,
      injections: [
        withPostSuccess((_, { data }) => {
          if (values[FIELD_NAMES.REMEMBER]) {
            setLocalstorage(LOGIN_KEY, data.token);
          }
        }),
        withPostFailure((dispatch) => {
          dispatch(reset(FORM_NAME));
          throw new SubmissionError({
            _error: i18n.t("formError:login"),
          });
        }),
      ],
    };
  },
  get: () => {
    return {
      type: actions.GET,
      target: TARGET.dashboardData,
      service: getData,
      successSelector: ({ data }) => data.sort(dynamicsort(FILTER_KEYS.TECH)),
      failureSelector: () => i18n.t("formError:dashboard"),
    };
  },
};
