export const TARGET = {
  loginForm: "loginForm",
  dashboardData: "dashboardData",
};

export const TYPES = ["SEND", "GET"];

export const INITIAL_STATE = {
  [TARGET.loginForm]: null,
  [TARGET.dashboardData]: null,
};
