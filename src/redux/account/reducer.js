import { completeState, completeReducer, createReducer } from "redux-recompose";
import { actions } from "./actions";
import { INITIAL_STATE } from "./constants";

const initialState = completeState(INITIAL_STATE);

const reducerDescription = {
  primaryActions: [actions.SEND, actions.GET],
};

const reducer = createReducer(
  initialState,
  completeReducer(reducerDescription)
);

export default reducer;
