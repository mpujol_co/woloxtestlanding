import i18n from "i18next";

//Spanish
i18n.addResources("es", "login", {
  title: "Iniciar Sesión",
  emailLabel: "E-mail",
  passwordLabel: "Contraseña",
  rememberLabel: "Mantenerme conectado",
  emailPlaceholder: "Escribe tu e-mail",
  passwordPlaceholder: "Escribe tu contraseña",
  submitButton: "Enviar",
});

//English
i18n.addResources("en", "login", {
  title: "Login",
  emailLabel: "E-mail",
  passwordLabel: "Password",
  rememberLabel: "Keep me signed in",
  emailPlaceholder: "Write your e-mail",
  passwordPlaceholder: "Enter your password",
  submitButton: "Send",
});
