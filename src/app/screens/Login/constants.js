import i18n from "i18next";
import formValidations from "~utils/formValidations";

export const FORM_NAME = "login";

export const FIELD_NAMES = {
  EMAIL: "email",
  PASSWORD: "password",
  REMEMBER: "remember",
};

export const VALIDATIONS = {
  [FIELD_NAMES.PASSWORD]: [
    formValidations.required,
    formValidations.maxLength(15),
    formValidations.minLength(7),
    formValidations.noWhiteSpaces,
  ],
  [FIELD_NAMES.EMAIL]: [formValidations.required, formValidations.email],
};

export const INPUT_LIST = [
  {
    label: i18n.t("login:emailLabel"),
    name: FIELD_NAMES.EMAIL,
    placeholder: i18n.t("login:emailPlaceholder"),
    validate: VALIDATIONS[FIELD_NAMES.EMAIL],
  },
  {
    label: i18n.t("login:passwordLabel"),
    name: FIELD_NAMES.PASSWORD,
    placeholder: i18n.t("login:passwordPlaceholder"),
    validate: VALIDATIONS[FIELD_NAMES.PASSWORD],
    type: "password",
  },
  {
    label: i18n.t("login:rememberLabel"),
    name: FIELD_NAMES.REMEMBER,
    type: "checkbox",
  },
];
