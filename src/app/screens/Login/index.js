import React, { useContext } from "react";
import i18n from "i18next";
import cn from "classnames";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ThemeContext } from "~contexts";
import { validateForm } from "redux-form-validators";
import { actionCreators as accountActions } from "~redux/account/actions";
import { func, bool, object } from "prop-types";
import { getPathname } from "~utils";
import { ROUTES_IDS } from "~constants/routes";
import { FORM_NAME, VALIDATIONS } from "./constants";
import styles from "./styles.module.scss";
import FormContainer from "./components/FormContainer";

function Login({ sendData, loading, history }) {
  const { setIsPrivate } = useContext(ThemeContext);

  const onSubmitSucceded = () => {
    history.push(getPathname(ROUTES_IDS.DASHBOARD));
    setIsPrivate(true);
  };

  return (
    <section className={cn(styles.section, "visible")}>
      <h2 className={styles.title}>{i18n.t("login:title")}</h2>
      <FormContainer
        form={FORM_NAME}
        onSubmit={sendData}
        loading={loading}
        validate={validateForm(VALIDATIONS)}
        onSubmitSucceded={onSubmitSucceded}
      />
    </section>
  );
}

Login.propTypes = {
  sendData: func,
  loading: bool,
  history: object,
};

const mapStateToProps = (state) => ({
  loading: state.account.loginFormLoading,
});

const mapDispatchToProps = (dispatch) => ({
  sendData: (values) => dispatch(accountActions.send(values)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
