import React, { useEffect } from "react";
import i18n from "i18next";
import { Field, reduxForm } from "redux-form";
import { func, bool, string, object } from "prop-types";
import FormFields from "~components/FormFields";
import Button from "~components/Button";
import { INPUT_LIST } from "../../constants";
import styles from "../../styles.module.scss";

function FormContainer({
  handleSubmit,
  loading,
  error,
  onSubmitSucceded,
  submitSucceeded,
}) {
  useEffect(() => {
    if (submitSucceeded) {
      onSubmitSucceded();
    }
  }, [submitSucceeded]);

  return (
    <form onSubmit={handleSubmit} className={styles.formContainer}>
      <div>
        {INPUT_LIST.map((item) => (
          <Field
            key={item.name}
            label={item.label}
            name={item.name}
            component={FormFields.InputField}
            placeholder={item.placeholder}
            validate={item.validate}
            type={item.type}
          />
        ))}
      </div>
      <div className={styles.buttonContainer}>
        <Button
          text={i18n.t("login:submitButton")}
          className={styles.submitButton}
          disabled={loading}
        />
        {error && <small className="error">{error}</small>}
      </div>
    </form>
  );
}

FormContainer.propTypes = {
  handleSubmit: func,
  loading: bool,
  error: string,
  submitSucceeded: bool,
  history: object,
  onSubmitSucceded: func,
};

export default reduxForm()(FormContainer);
