import React, { useState, createRef, useEffect } from "react";
import i18n from "i18next";
import { func, arrayOf, object, string } from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import InputField from "~components/FormFields/components/InputField";
import { FILTER_KEYS } from "~constants/order";
import { sortArray } from "~screens/Dashboard/utils";
import styles from "./styles.module.scss";

function Filter({ setFilteredData, data, orderType }) {
  const [showClearButton, setShowClearButton] = useState(false);
  const inputRef = createRef();

  useEffect(() => {
    resetData();
  }, [data]);

  const filterData = ({ target: { value } }) => {
    const NEW_DATA = value.length
      ? data.filter((item) =>
          Object.keys(item).some((key) =>
            Object.values(FILTER_KEYS).find(
              (i) =>
                i === key &&
                item[key].toLowerCase().includes(value.toLowerCase())
            )
          )
        )
      : data;
    setShowClearButton(!!value.length);
    setFilteredData(sortArray(NEW_DATA, orderType));
  };

  const resetData = () => {
    inputRef.current.value = "";
    setShowClearButton(false);
  };

  const clearInput = () => {
    resetData();
    setFilteredData(sortArray(data, orderType));
  };

  return (
    <div className={styles.filterContainer}>
      <InputField
        label={i18n.t("dashboard:filterLabel")}
        placeholder={i18n.t("dashboard:filterPlaceholder")}
        input={{ ref: inputRef, onChange: filterData }}
        className={styles.input}
      />
      {showClearButton && (
        <FontAwesomeIcon
          icon={faTimes}
          onClick={clearInput}
          className={styles.clearButton}
        />
      )}
    </div>
  );
}

Filter.propTypes = {
  setFilteredData: func,
  data: arrayOf(object),
  orderType: string,
};

export default Filter;
