import i18n from "i18next";
import { ORDER_TYPE } from "~constants/order";

export const OPTIONS = [
  { value: ORDER_TYPE.ASC, name: i18n.t("dashboard:ascending") },
  { value: ORDER_TYPE.DESC, name: i18n.t("dashboard:descending") },
];
