import React from "react";
import i18n from "i18next";
import { func, arrayOf, object } from "prop-types";
import SelectField from "~components/SelectField";
import { sortArray } from "~screens/Dashboard/utils";
import { OPTIONS } from "./constants";

function Order({ setFilteredData, filteredData, setOrderType }) {
  const handleChange = ({ target: { value } }) => {
    setOrderType(value);
    setFilteredData(sortArray(filteredData, value));
  };

  return (
    <SelectField
      label={i18n.t("dashboard:orderLabel")}
      options={OPTIONS}
      onChange={handleChange}
    />
  );
}

Order.propTypes = {
  setFilteredData: func,
  filteredData: arrayOf(object),
  setOrderType: func,
};

export default Order;
