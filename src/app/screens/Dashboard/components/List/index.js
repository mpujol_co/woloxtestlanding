import React from "react";
import i18n from "i18next";
import cn from "classnames";
import { func, bool, object, arrayOf, string } from "prop-types";
import Loader from "~components/Loader";
import styles from "./styles.module.scss";

function List({ filteredData, data, loading, error }) {
  return loading || !filteredData ? (
    <Loader />
  ) : error ? (
    <h3 className={cn("error", styles.error)}>{error}</h3>
  ) : (
    <>
      <div className={styles.listContainer}>
        {filteredData.map(
          ({ logo, tech, author, year, type, language, license }) => (
            <div key={tech} className={styles.listElement}>
              <h3 className={styles.tech}>{tech}</h3>
              <img className={styles.logo} src={logo} alt={tech} />
              <p className={styles.author}>{author}</p>
              <small className={styles.year}>
                {year} - {license}
              </small>
              <p className={styles.type}>{type}</p>
              <p className={styles.language}>{language}</p>
            </div>
          )
        )}
      </div>
      <p className={styles.counter}>
        {i18n.t("dashboard:counter", {
          filter: filteredData.length,
          total: data.length,
        })}
      </p>
    </>
  );
}

List.propTypes = {
  getData: func,
  loading: bool,
  filteredData: arrayOf(object),
  data: arrayOf(object),
  error: string,
};

export default List;
