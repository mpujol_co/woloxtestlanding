import React, { useContext } from "react";
import i18n from "i18next";
import { withRouter } from "react-router-dom";
import { object } from "prop-types";
import { remove as removeLocalstorage } from "~services/localstorage";
import { LOGIN_KEY } from "~constants/localstorage";
import { ThemeContext } from "~contexts";
import { getPathname } from "~utils";
import { ROUTES_IDS } from "~constants/routes";
import Button from "~components/Button";
import styles from "./styles.module.scss";

function Title({ history }) {
  const { setIsPrivate } = useContext(ThemeContext);

  const onLogout = () => {
    history.push(getPathname(ROUTES_IDS.HOME));
    removeLocalstorage(LOGIN_KEY);
    setIsPrivate(false);
  };

  return (
    <div className={styles.titleContainer}>
      <h2 className={styles.title}>{i18n.t("dashboard:title")}</h2>
      <Button
        className={styles.logoutButton}
        text={i18n.t("dashboard:logout")}
        onClick={onLogout}
      />
    </div>
  );
}

Title.propTypes = {
  history: object,
};

export default withRouter(Title);
