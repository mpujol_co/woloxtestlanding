import { FILTER_KEYS } from "~constants/order";
import { dynamicsort } from "~utils";

export const sortArray = (array, type) =>
  [...array].sort(dynamicsort(FILTER_KEYS.TECH, type));
