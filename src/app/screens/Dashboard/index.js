import React, { useEffect, useState } from "react";
import { func, bool, object, arrayOf, string } from "prop-types";
import cn from "classnames";
import { connect } from "react-redux";
import { actionCreators as accountActions } from "~redux/account/actions";
import { ORDER_TYPE } from "~constants/order";
import Filter from "./components/Filter";
import Order from "./components/Order";
import Title from "./components/Title";
import List from "./components/List";
import styles from "./styles.module.scss";

function Dashboard({ getData, data, loading, error }) {
  const [filteredData, setFilteredData] = useState(data);
  const [orderType, setOrderType] = useState(ORDER_TYPE.ASC);

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => setFilteredData(data), [data]);

  return (
    <section className={cn(styles.section, "visible")}>
      <Title />
      <div className={styles.optionsContainer}>
        <Filter
          setFilteredData={setFilteredData}
          orderType={orderType}
          data={data}
        />
        <Order
          setFilteredData={setFilteredData}
          filteredData={filteredData}
          setOrderType={setOrderType}
        />
      </div>
      <List
        filteredData={filteredData}
        data={data}
        loading={loading}
        error={error}
      />
    </section>
  );
}

Dashboard.propTypes = {
  getData: func,
  loading: bool,
  data: arrayOf(object),
  error: string,
};

const mapStateToProps = (state) => ({
  data: state.account.dashboardData,
  error: state.account.dashboardDataError,
  loading: state.account.dashboardDataLoading,
});

const mapDispatchToProps = (dispatch) => ({
  getData: () => dispatch(accountActions.get()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
