import i18n from "i18next";

//Spanish
i18n.addResources("es", "dashboard", {
  title: "Listado",
  logout: "Cerrar sesión",
  filterLabel: "Filtro",
  filterPlaceholder: "Ingresa el texto a filtrar ...",
  counter: "Mostrando {{filter}} de {{total}} tecnologías",
  orderLabel: "Ordenar",
  ascending: "Ascendente",
  descending: "Descendente",
});

//English
i18n.addResources("en", "dashboard", {
  title: "List",
  logout: "Logout",
  filterLabel: "Filter",
  filterPlaceholder: "Enter the text to filter ...",
  counter: "Showing {{filter}} of {{total}} technologies",
  orderLabel: "Order",
  ascending: "Ascending",
  descending: "Descending",
});
