import React from "react";
import i18n from "i18next";
import cn from "classnames";
import { customRefProptype } from "~proptypes/general";
import IMG_HERO from "~assets/Img Hero/Ic_ilustra_Hero.png";
import IMG_HERO_2X from "~assets/Img Hero/Ic_ilustra_Hero@2x.png";
import IMG_HERO_3X from "~assets/Img Hero/Ic_ilustra_Hero@3x.png";
import styles from "./styles.module.scss";

function Home({ customRef }) {
  return (
    <section ref={customRef} className={cn(styles.section, "visible")}>
      <h1
        className={styles.text}
        dangerouslySetInnerHTML={{ __html: i18n.t("home:text") }}
      />
      <div className={styles.imageContainer}>
        <img
          srcSet={`${IMG_HERO}, ${IMG_HERO_2X} 2x, ${IMG_HERO_3X} 3x`}
          alt=""
        />
      </div>
    </section>
  );
}

Home.propTypes = {
  customRef: customRefProptype,
};

export default Home;
