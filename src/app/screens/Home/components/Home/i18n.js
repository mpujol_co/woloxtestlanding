import i18n from "i18next";

//Spanish
i18n.addResources("es", "home", {
  text: "Bienvenido a tu <b>entrevista técnica</b> en <span>Wolox</span>",
});

//English
i18n.addResources("en", "home", {
  text: "Welcome to your <b>technical interview</b> in <span>Wolox</span>",
});
