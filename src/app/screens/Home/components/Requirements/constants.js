import i18n from "i18next";
import BULLET_GREEN from "~assets/Ic_Bullet_1.svg";
import BULLET_BLACK from "~assets/Ic_Bullet_2.svg";
import BULLET_BLUE from "~assets/Ic_Bullet_3.svg";

const REQUERIMENTS_IDS = {
  STUDENTS: "students",
  ENGLISH: "english",
  KNOWLEDGEMENT: "knowledgement",
};

export const REQUERIMENTS = [
  {
    id: REQUERIMENTS_IDS.STUDENTS,
    image: BULLET_GREEN,
    name: i18n.t("requirements:req1"),
  },
  {
    id: REQUERIMENTS_IDS.ENGLISH,
    image: BULLET_BLACK,
    name: i18n.t("requirements:req2"),
  },
  {
    id: REQUERIMENTS_IDS.KNOWLEDGEMENT,
    image: BULLET_BLUE,
    name: i18n.t("requirements:req3"),
  },
];
