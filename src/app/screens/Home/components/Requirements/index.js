import React from "react";
import i18n from "i18next";
import { customRefProptype } from "~proptypes/general";
import styles from "./styles.module.scss";
import { REQUERIMENTS } from "./constants";

function Requirements({ customRef }) {
  return (
    <section ref={customRef} className={styles.section}>
      <h2 className={styles.title}>{i18n.t("requirements:title")}</h2>
      <div className={styles.listContainer}>
        {REQUERIMENTS.map(({ id, image, name }) => (
          <div key={id} className={styles.listElement}>
            <img src={image} alt={name} />
            <p>{name}</p>
          </div>
        ))}
      </div>
    </section>
  );
}

Requirements.propTypes = {
  customRef: customRefProptype,
};

export default Requirements;
