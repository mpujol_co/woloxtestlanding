import i18n from "i18next";

//Spanish
i18n.addResources("es", "requirements", {
  title: "Requerimientos",
  req1:
    "Estudiantes avanzados o recibidos de carreras del rubro informático, sistemas o electrónicos.",
  req2: "Inglés intermedio/avanzado",
  req3: "Conocimiento en metodologías ágiles (deseable)",
});

//English
i18n.addResources("en", "requirements", {
  title: "Requirements",
  req1:
    "Advanced or received students from careers in the computer, systems or electronic field.",
  req2: "Intermediate / Advanced English",
  req3: "Knowledge of agile methodologies (desirable)",
});
