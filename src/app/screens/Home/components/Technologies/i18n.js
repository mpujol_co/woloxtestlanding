import i18n from "i18next";

//Spanish
i18n.addResources("es", "technologies", {
  imageAlt: "Tecnologías",
  text:
    "Estamos buscando para incorporar gente increíble para estas tecnologías:",
  twitterTitle: "350 + <span>Woloxers</span>",
  buttonText: "Síguenos",
  rightText:
    "Trabajamos para <b>convertir <span>ideas</span></b> en productos.",
});

//English
i18n.addResources("en", "technologies", {
  imageAlt: "Technologies",
  text: "We are looking to recruit amazing people for these technologies:",
  twitterTitle: "350 + <span>Woloxers</span>",
  buttonText: "Follow us",
  rightText: "We work to <b>convert <span>ideas</span></b> into products.",
});
