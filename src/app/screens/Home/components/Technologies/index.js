import React from "react";
import i18n from "i18next";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTwitter } from "@fortawesome/free-brands-svg-icons";
import { customRefProptype } from "~proptypes/general";
import IMG_TECHNOLOGIES from "~assets/Ic_Tecnologys.svg";
import Button from "~components/Button";
import styles from "./styles.module.scss";

function Technologies({ customRef }) {
  return (
    <>
      <section ref={customRef} className={styles.section}>
        <h2 className={styles.text}>{i18n.t("technologies:text")}</h2>
        <div className={styles.imageContainer}>
          <img src={IMG_TECHNOLOGIES} alt={i18n.t("technologies:imageAlt")} />
        </div>
      </section>
      <div className={styles.banner}>
        <div className={styles.twitterContainer}>
          <h1
            className={styles.twitterTitle}
            dangerouslySetInnerHTML={{
              __html: i18n.t("technologies:twitterTitle"),
            }}
          />
          <div className={styles.iconContainer}>
            <FontAwesomeIcon icon={faTwitter} />
            <p>@Wolox</p>
          </div>
          <a
            href="https://twitter.com/wolox"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Button
              text={i18n.t("technologies:buttonText")}
              className={styles.button}
            />
          </a>
        </div>
        <div className={styles.textContainer}>
          <h1
            dangerouslySetInnerHTML={{
              __html: i18n.t("technologies:rightText"),
            }}
          />
        </div>
      </div>
    </>
  );
}

Technologies.propTypes = {
  customRef: customRefProptype,
};

export default Technologies;
