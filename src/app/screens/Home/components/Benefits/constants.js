import i18n from "i18next";
import HOURS from "~assets/Ic_Hour.svg";
import HOME_OFFICE from "~assets/Ic_HomeOffice.svg";
import WORKSHOPS from "~assets/Ic_Workshops.svg";
import SNACKS from "~assets/Ic_DrinkSnacks.svg";
import REMOTE_WEEK from "~assets/Ic_laptop.svg";
import TECHNOLOGIES from "~assets/Ic_brain.svg";

const BENEFITS_IDS = {
  HOURS: "hours",
  HOME_OFFICE: "homeOffice",
  WORKSHOPS: "workshops",
  SNACKS: "snacks",
  REMOTE_WEEK: "remoteWeek",
  TECHNOLOGIES: "technologies",
};

export const BENEFITS = [
  {
    id: BENEFITS_IDS.HOURS,
    image: HOURS,
    name: i18n.t("benefits:hoursFlexibility"),
  },
  {
    id: BENEFITS_IDS.HOME_OFFICE,
    image: HOME_OFFICE,
    name: i18n.t("benefits:homeOffice"),
  },
  {
    id: BENEFITS_IDS.WORKSHOPS,
    image: WORKSHOPS,
    name: i18n.t("benefits:workshops"),
  },
  {
    id: BENEFITS_IDS.SNACKS,
    image: SNACKS,
    name: i18n.t("benefits:snacks"),
  },
  {
    id: BENEFITS_IDS.REMOTE_WEEK,
    image: REMOTE_WEEK,
    name: i18n.t("benefits:remoteWeek"),
  },
  {
    id: BENEFITS_IDS.TECHNOLOGIES,
    image: TECHNOLOGIES,
    name: i18n.t("benefits:technologies"),
  },
];
