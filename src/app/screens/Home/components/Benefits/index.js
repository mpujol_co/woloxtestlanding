import React from "react";
import i18n from "i18next";
import { customRefProptype } from "~proptypes/general";
import { BENEFITS } from "./constants";
import styles from "./styles.module.scss";

function Benefits({ customRef }) {
  return (
    <section ref={customRef} className={styles.section}>
      <h2 className={styles.title}>
        {i18n.t("benefits:title")} <span>;)</span>
      </h2>
      <div className={styles.listContainer}>
        {BENEFITS.map(({ id, image, name }) => (
          <div key={id} className={styles.listElement}>
            <img src={image} alt={name} />
            <h3>{name}</h3>
          </div>
        ))}
      </div>
    </section>
  );
}

Benefits.propTypes = {
  customRef: customRefProptype,
};

export default Benefits;
