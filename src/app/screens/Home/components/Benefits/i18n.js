import i18n from "i18next";

//Spanish
i18n.addResources("es", "benefits", {
  title: "Entre los beneficios que ofrecemos se encuentran",
  hoursFlexibility: "Flexibilidad horaria",
  homeOffice: "Home office",
  workshops: "Capacitaciones y workshops",
  snacks: "Snacks, frutas y bebidas gratis",
  remoteWeek: "Semana remota",
  technologies: "Trabajar en últimas tecnologías",
});

//English
i18n.addResources("en", "benefits", {
  title: "Among the benefits we offer are",
  hoursFlexibility: "Flexible schedule",
  workshops: "Trainings and workshops",
  snacks: "Free snacks, fruits and drinks",
  remoteWeek: "Remote week",
  technologies: "Work on latest technologies",
});
