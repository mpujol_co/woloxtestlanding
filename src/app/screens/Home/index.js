import React, { useContext, useEffect } from "react";
import { func, string } from "prop-types";
import { ThemeContext } from "~contexts";
import { SECTIONS, REFERENCES, SECTIONS_IDS } from "~constants/homeSections";

function Home() {
  const { sectionActive, setSectionActive } = useContext(ThemeContext);

  useEffect(() => {
    let positionTop = 0;

    const handleScroll = (event) => {
      positionTop = -1 * event.srcElement.body.getBoundingClientRect().top;
      let active = sectionActive;
      SECTIONS.forEach(({ id }) => {
        if (positionTop >= REFERENCES[id].current.offsetTop) {
          active = id;
        }
      });
      setSectionActive(active);
    };
    setSectionActive(SECTIONS_IDS.HOME);

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      {SECTIONS.map(({ id, ref, component: Element }) => (
        <Element key={id} customRef={ref} />
      ))}
    </>
  );
}

Home.propTypes = {
  sectionActive: string,
  setSectionActive: func,
};

export default Home;
