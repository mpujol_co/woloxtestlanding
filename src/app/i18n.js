import i18n from "i18next";

//Spanish
i18n.addResources("es", "formError", {
  required: "El campo es requerido.",
  email: "El e-mail es invalido.",
  maxLength: "El campo debe tener máximo {{max}} caracteres.",
  minLength: "El campo debe tener mínimo {{min}} caracteres.",
  noWhiteSpaces: "Caracteres de espacio al inicio y/o al final no permitidos.",
  login: "El campo e-mail o contraseña es incorrecto. Inténtalo nuevamente.",
  dashboard: "Hubo un error al cargar los datos. Inténtalo nuevamente.",
});

//English
i18n.addResources("en", "formError", {
  required: "The field is required.",
  email: "The email is invalid.",
  maxLength: "The field must have a maximum of {{max}} characters.",
  minLength: "The field must be at least {{min}} characters.",
  noWhiteSpaces:
    "Space characters at the beginning and / or at the end are not allowed.",
  login: "The e-mail or password field is incorrect. Please try again.",
  dashboard: "There was an error loading the data. Please try again.",
});
