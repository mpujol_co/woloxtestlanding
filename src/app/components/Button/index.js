import React from "react";
import { string, bool } from "prop-types";
import cn from "classnames";
import styles from "./styles.module.scss";

function Button({ text, className, disabled, ...props }) {
  return (
    <button
      className={cn(styles.button, {
        [className]: !!className,
        [styles.disabled]: disabled,
      })}
      {...props}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  text: string.isRequired,
  className: string,
  disabled: bool,
};

export default Button;
