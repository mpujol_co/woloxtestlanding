import React from "react";
import cn from "classnames";
import { inputProptype } from "~proptypes/general";
import styles from "./styles.module.scss";

function InputField({ label, type, placeholder, meta, input, className }) {
  return (
    <label
      className={cn(styles.inputContainer, {
        [styles.checkbox]: type === "checkbox",
        [className]: !!className,
      })}
    >
      {label}
      {type === "checkbox" && (
        <div
          className={cn(styles.checkmark, {
            [styles.checked]: input.checked,
          })}
        ></div>
      )}
      <input
        {...input}
        type={type}
        placeholder={placeholder}
        className={cn({ [styles.error]: meta?.touched && meta?.error })}
      />
      {meta?.touched && meta?.error && (
        <small className="error">{meta.error}</small>
      )}
    </label>
  );
}

InputField.defaultProps = {
  type: "text",
  placeholder: "",
};

InputField.propTypes = {
  ...inputProptype,
};

export default InputField;
