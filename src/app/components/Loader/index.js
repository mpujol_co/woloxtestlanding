import React, { useEffect, useContext } from "react";
import cn from "classnames";
import { bool } from "prop-types";
import Loader from "react-loader-spinner";
import { ThemeContext } from "~contexts";
import { BLUE_COLOR } from "~styles/colors.scss";
import styles from "./styles.module.scss";

function LoaderContainer({ fullHeight }) {
  const { setLoaderFinished } = useContext(ThemeContext);

  useEffect(() => {
    setLoaderFinished(false);
    return () => {
      setLoaderFinished(true);
    };
  }, []);

  return (
    <div className={cn(styles.section, { [styles.fullHeight]: fullHeight })}>
      <Loader type="Puff" color={BLUE_COLOR} height={100} width={100} />
    </div>
  );
}

LoaderContainer.propTypes = {
  fullHeight: bool,
};

export default LoaderContainer;
