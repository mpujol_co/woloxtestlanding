import React from "react";
import cn from "classnames";
import { arrayOf, shape, string, func } from "prop-types";
import styles from "./styles.module.scss";

function SelectField({ label, className, options, onChange, value }) {
  return (
    <div className={cn(styles.selectContainer, { [className]: !!className })}>
      <label className={styles.label}>{label}</label>
      <select onChange={onChange} className={styles.select} value={value}>
        {options.map(({ value, name }) => (
          <option key={value} value={value}>
            {name}
          </option>
        ))}
      </select>
    </div>
  );
}

SelectField.propTypes = {
  label: string,
  className: string,
  onChange: func,
  value: string,
  options: arrayOf(
    shape({
      value: string,
      name: string,
    })
  ),
};

export default SelectField;
