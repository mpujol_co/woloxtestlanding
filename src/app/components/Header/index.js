import React, { useState, useEffect, createRef } from "react";
import cn from "classnames";
import Menu from "./components/Menu";
import styles from "./styles.module.scss";

function Header() {
  const [isFixed, setFixed] = useState(false);
  const [headerHeight, setHeaderHeight] = useState(0);
  const headerRef = createRef();

  useEffect(() => {
    setHeaderHeight(headerRef.current && headerRef.current.clientHeight);
  });

  useEffect(() => {
    const handleScroll = (event) => {
      const position = -event.srcElement.body.getBoundingClientRect().top;
      setFixed(position > headerHeight + 40);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [isFixed, headerRef]);

  return (
    <header
      ref={headerRef}
      className={cn([styles.header], { [styles.fixedHeader]: isFixed })}
    >
      <Menu headerHeight={headerHeight} />
    </header>
  );
}

export default Header;
