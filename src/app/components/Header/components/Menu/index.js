import React, { useContext, useState, useEffect } from "react";
import i18n from "i18next";
import { withRouter } from "react-router-dom";
import { Link, useLocation } from "react-router-dom";
import cn from "classnames";
import { number, object } from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faTimes } from "@fortawesome/free-solid-svg-icons";
import { ThemeContext } from "~contexts";
import Button from "~components/Button";
import LOGO from "~assets/logo_full_color.svg";
import { ROUTES_IDS } from "~constants/routes";
import { SECTIONS } from "~constants/homeSections";
import DropdownLanguage from "../DropdownLanguage";
import styles from "../../styles.module.scss";
import { scrollToId, getPathname } from "~utils";

function Menu({ headerHeight, history }) {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const { sectionActive, isPrivate } = useContext(ThemeContext);
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  const handleMenuClick = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <>
      <Link to={getPathname(ROUTES_IDS.HOME)} className={styles.logo}>
        <img src={LOGO} alt={i18n.t("sectionTitle:logoAlt")} />
      </Link>
      <div className={styles.menuIcon} onClick={handleMenuClick}>
        <FontAwesomeIcon icon={isMenuOpen ? faTimes : faBars} />
      </div>
      <div
        className={cn(styles.menuList, { [styles.isOpen]: isMenuOpen })}
        style={{ top: `${headerHeight}px` }}
      >
        {pathname === getPathname(ROUTES_IDS.HOME) &&
          SECTIONS.map(
            ({ id, name }) =>
              name && (
                <Link
                  to={getPathname(ROUTES_IDS.HOME)}
                  key={id}
                  className={cn({
                    [styles.active]: sectionActive === id,
                  })}
                  onClick={scrollToId(id)}
                >
                  <h3>{name}</h3>
                </Link>
              )
          )}
        <Link
          to={
            history.location.pathname === getPathname(ROUTES_IDS.HOME)
              ? isPrivate
                ? getPathname(ROUTES_IDS.DASHBOARD)
                : getPathname(ROUTES_IDS.LOGIN)
              : getPathname(ROUTES_IDS.HOME)
          }
        >
          <Button
            className={styles.loginButton}
            text={
              history.location.pathname === getPathname(ROUTES_IDS.HOME)
                ? isPrivate
                  ? i18n.t("sectionTitle:dashboard")
                  : i18n.t("sectionTitle:login")
                : i18n.t("sectionTitle:back")
            }
          />
        </Link>
        <DropdownLanguage />
      </div>
    </>
  );
}

Menu.propTypes = {
  headerHeight: number.isRequired,
  history: object,
};

export default withRouter(Menu);
