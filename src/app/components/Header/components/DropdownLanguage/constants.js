import i18n from "i18next";

export const LANGUAGES = [
  {
    value: "es",
    name: i18n.t("languages:es"),
  },
  {
    value: "en",
    name: i18n.t("languages:en"),
  },
];
