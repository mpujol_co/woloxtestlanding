import React from "react";
import i18n from "i18next";
import { setLanguage, getLanguage } from "~utils/language";
import SelectField from "~components/SelectField";
import { LANGUAGES } from "./constants";
import styles from "./styles.module.scss";

function DropdownLanguage() {
  const handleChange = ({ target: { value } }) => {
    setLanguage(value);
  };

  return (
    <SelectField
      label={i18n.t("languages:title")}
      options={LANGUAGES}
      onChange={handleChange}
      value={getLanguage()}
      className={styles.dropdown}
    />
  );
}

export default DropdownLanguage;
