import i18n from "i18next";

// Spanish
i18n.addResources("es", "sectionTitle", {
  logoAlt: "WOLOX",
  home: "Inicio",
  technologies: "Tecnologías",
  benefits: "Beneficios",
  requirements: "Requerimientos",
  login: "Iniciar sesión",
  dashboard: "Listado",
  back: "Volver",
});

i18n.addResources("es", "languages", {
  title: "Idioma",
  es: "ES",
  en: "EN",
});

//English
i18n.addResources("en", "sectionTitle", {
  logoAlt: "WOLOX",
  home: "Home",
  technologies: "Technologies",
  benefits: "Benefits",
  requirements: "Requirements",
  login: "Login",
  dashboard: "List",
  back: "Back",
});

i18n.addResources("en", "languages", {
  title: "Language",
});
