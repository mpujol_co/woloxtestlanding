import i18n from "i18next";

// Spanish
i18n.addResources("es", "footer", {
  text1: "Gracias por <span>completar el ejercicio</span>",
  text2: "Te invitamos a ver mas información",
  buttonText: "Conocer más",
});

//English
i18n.addResources("en", "footer", {
  text1: "Thank you for <span>completing the exercise</span>",
  text2: "We invite you to see more information",
  buttonText: "Know more",
});
