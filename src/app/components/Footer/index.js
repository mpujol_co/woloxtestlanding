import React from "react";
import i18n from "i18next";
import Button from "~components/Button";
import IMG_TECHNOLOGIES from "~assets/Ic_Wolox_Footer.svg";
import styles from "./styles.module.scss";

function Footer() {
  return (
    <footer>
      <section className={styles.footerSection}>
        <h1
          className={styles.thanksText}
          dangerouslySetInnerHTML={{ __html: i18n.t("footer:text1") }}
        />
        <h2
          className={styles.inviteText}
          dangerouslySetInnerHTML={{ __html: i18n.t("footer:text2") }}
        />
        <a
          href="https://www.wolox.com.ar/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Button
            text={i18n.t("footer:buttonText")}
            className={styles.button}
          />
        </a>
        <img
          className={styles.logo}
          src={IMG_TECHNOLOGIES}
          alt={i18n.t("sectionTitle:logoAlt")}
        />
      </section>
    </footer>
  );
}

export default Footer;
