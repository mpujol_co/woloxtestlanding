import React, { useState, Suspense, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { ThemeContext } from "~contexts";
import { getPathname, fadeUpScroll } from "~utils";
import { get as getLocalstorage } from "~services/localstorage";
import { LOGIN_KEY } from "~constants/localstorage";
import { ROUTES, ROUTES_IDS } from "~constants/routes";
import { SECTIONS_IDS } from "~constants/homeSections";
import Header from "~components/Header";
import Footer from "~components/Footer";
import Loader from "~components/Loader";

function App() {
  const [sectionActive, setSectionActive] = useState(SECTIONS_IDS.HOME);
  const [isPrivate, setIsPrivate] = useState(!!getLocalstorage(LOGIN_KEY));
  const [loaderFinished, setLoaderFinished] = useState(false);

  useEffect(() => {
    let positionTop = 0;
    const handleScroll = (event) => {
      positionTop = -1 * event.srcElement.body.getBoundingClientRect().top;

      fadeUpScroll(positionTop);
    };
    fadeUpScroll();

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [loaderFinished]);

  return (
    <ThemeContext.Provider
      value={{
        sectionActive,
        setSectionActive,
        isPrivate,
        setIsPrivate,
        setLoaderFinished,
      }}
    >
      <div className="App">
        <Router>
          <Header />
          <main>
            <Suspense fallback={<Loader fullHeight />}>
              <Switch>
                {ROUTES.map(
                  ({
                    id,
                    path,
                    isPrivate: isPrivateRoute,
                    component: Element,
                  }) =>
                    (!!isPrivateRoute === isPrivate ||
                      id === ROUTES_IDS.HOME) && (
                      <Route key={id} path={path} exact>
                        <Element />
                      </Route>
                    )
                )}
                <Route>
                  <Redirect to={getPathname(ROUTES_IDS.HOME)} push />
                </Route>
              </Switch>
            </Suspense>
          </main>
          <Footer />
        </Router>
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
