import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "~config/i18n";
import "~styles/index.scss";
import App from "~app";
import * as serviceWorker from "serviceWorker";
import store from "~redux";

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
